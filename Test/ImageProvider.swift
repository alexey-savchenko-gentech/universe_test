//
//  ImageProvider.swift
//  Test
//
//  Created by Alexey Savchenko on 26.06.2020.
//  Copyright © 2020 Universe. All rights reserved.
//

import Foundation
import RxSwift

class ImageProvider {
  
  func getImage(index: Int) -> Observable<UIImage> {
    return Observable<UIImage>.create { observer in
      
      let renderSize = CGSize(width: 2000, height: 2000)
      let rect = CGRect(origin: .zero, size: renderSize)
      let renderer = UIGraphicsImageRenderer(size: renderSize)
      let image = renderer.image { (ctx) in
        UIColor.white.setFill()
        let circlePath = UIBezierPath(ovalIn: rect)
        ctx.cgContext.setLineWidth(10)
        ctx.cgContext.addPath(circlePath.cgPath)
        UIColor.red.setStroke()
        circlePath.stroke()
        
        let string = NSString(string: "\(index)")
        string.draw(at: .init(x: rect.midX, y: rect.midY),
                    withAttributes: [.foregroundColor: UIColor.black,
                                     .font: UIFont.systemFont(ofSize: 30, weight: .bold)])
      }
      
      observer.onNext(image)
      
      return Disposables.create()
    }
  }
  
}
